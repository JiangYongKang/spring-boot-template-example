package com.vincent.template.example.model;

import lombok.Data;

import java.util.Date;

/**
 * Author: vincent
 * Date: 2018-11-08 21:08:00
 * Comment:
 */

@Data
public class Message {

    private Integer id;
    private String nickname;
    private String content;
    private Date createdTime;
    private Date updatedTime;

}
