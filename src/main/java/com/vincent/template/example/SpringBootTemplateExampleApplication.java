package com.vincent.template.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTemplateExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTemplateExampleApplication.class, args);
    }
}
