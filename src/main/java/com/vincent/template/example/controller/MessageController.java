package com.vincent.template.example.controller;

import com.vincent.template.example.service.MessageService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Author: vincent
 * Date: 2018-11-08 21:07:00
 * Comment:
 */

@RestController
@RequestMapping("/messages")
public class MessageController {

    @Resource
    private MessageService messageService;

    @GetMapping
    public ResponseEntity<?> index() {
        return ResponseEntity.ok(messageService.fetchAllMessages());
    }

}
