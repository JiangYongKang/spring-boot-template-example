package com.vincent.template.example.service;

import com.vincent.template.example.model.Message;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: vincent
 * Date: 2018-11-08 21:07:00
 * Comment:
 */

@Service
public class MessageService {

    @Resource
    private JdbcTemplate template;

    public List<Message> fetchAllMessages() {
        return template.query("select * from message", (result, i) -> {
            Message message = new Message();
            message.setId(result.getInt("id"));
            message.setNickname(result.getString("nickname"));
            message.setContent(result.getString("content"));
            message.setCreatedTime(result.getDate("created_time"));
            message.setUpdatedTime(result.getDate("updated_time"));
            return message;
        });
    }

}
