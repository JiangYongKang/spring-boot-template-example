drop table if exists message;
create table message (
  id           int auto_increment primary key,
  nickname     varchar(64)  not null,
  content      varchar(255) not null,
  created_time datetime     null,
  updated_time datetime     null
);

